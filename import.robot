*** Settings ***
Library     SeleniumLibrary

Resource    ${CURDIR}/keywords/features/login_feature.robot
Resource    ${CURDIR}/keywords/features/openCategories_feature.robot
Resource    ${CURDIR}/keywords/features/openPosterPage_feature.robot
Resource    ${CURDIR}/keywords/features/openTrees_feature.robot
Resource    ${CURDIR}/keywords/features/addItemToCart_feature.robot
Resource    ${CURDIR}/keywords/pages/verifyAllCost.robot