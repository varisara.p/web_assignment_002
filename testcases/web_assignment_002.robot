*** Settings ***
Resource        ${CURDIR}/../import.robot

*** Keywords ***
Open Browser For Test
    Open Browser	      http://testphp.vulnweb.com/login.php          browser=safari

*** Test Cases ***
Test For Web_assignment_002
    Open Browser For Test
    Sign In The Website                 test        test
    Open Categories Page                Browse categories
    Open Poster Page                    Posters
    Open Trees Page                     Trees   'painted by: '  Blad3
    Add Item To Cart
    Wait and Verify Cost
    
