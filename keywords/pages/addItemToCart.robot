*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${button}       xpath=//input[@value='add this picture to cart']

*** Keywords ****
Wait and Check That Have Button Add To Cart On Page
    Wait Until Element Is Visible  ${button}
   
Add The Picture To Cart
     Click Button	        ${button} 
   