*** Settings ***
Library     SeleniumLibrary

*** Keywords ****
Wait and Verify Cost
    Wait Until Element is Visible   xpath=//td[contains(text(),'$')] 
    ${data}=    Get Text            xpath=//td[contains(text(),'$')]        
    Element Should Contain        xpath=//h3[contains(text(),'Total: ')]          Total: ${data}
