*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${username_locator}         name=uname
${password_locator}         name=pass
${button_locator}           xpath=//input[@value='login']

*** Keywords ****
Input Username
    [Arguments]         ${input_value}
    Input Text          ${username_locator}     ${input_value}

Input Password
    [Arguments]         ${input_value}
    Input Text          ${password_locator}     ${input_value}

Click Sign In
    Click Button	    ${button_locator}
    Sleep               1