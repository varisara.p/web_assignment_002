*** Settings ***
Library     SeleniumLibrary

*** Variables ***
${categories_locators}         xpath=//li/a[contains(text(),'categories')]

*** Keywords ****
Check That Have Categories On Left Hand
    [Arguments]                     ${value}
    Element Should Contain          ${categories_locators}          ${value}

Open And Wait Website To Complete Response
    Click Link                      ${categories_locators}