*** Settings ***
Library     SeleniumLibrary

*** Keywords ****
Wait and Check That Have Posters On Page
    [Arguments]                     ${input_value}
    Wait Until Element is Visible   xpath=//h3[text()='${input_value}'] 
    Element Should Contain          xpath=//h3[text()='${input_value}']          ${input_value}

Open Poster Website To Complete Response
    [Arguments]                     ${input_value}
    Click Element                   xpath=//h3[text()='${input_value}']
   
