*** Settings ***
Library         SeleniumLibrary

Resource        ${CURDIR}/../pages/addItemToCart.robot

*** Keywords ***
Add Item To Cart
    addItemToCart.Wait and Check That Have Button Add To Cart On Page
    addItemToCart.Add The Picture To Cart