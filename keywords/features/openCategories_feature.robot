*** Settings ***
Library         SeleniumLibrary

Resource        ${CURDIR}/../pages/openCategories.robot

*** Keywords ***
Open Categories Page 
    [Arguments]         ${value}
    openCategories.Check That Have Categories On Left Hand          ${value}
    openCategories.Open And Wait Website To Complete Response