*** Settings ***
Library         SeleniumLibrary

Resource        ${CURDIR}/../pages/openPostersPage.robot

*** Keywords ***
Open Poster Page 
    [Arguments]         ${input_value}
    openPostersPage.Wait and Check That Have Posters On Page  ${input_value}
    openPostersPage.Open Poster Website To Complete Response  ${input_value}