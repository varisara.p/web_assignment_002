*** Settings ***
Library         SeleniumLibrary

Resource        ${CURDIR}/../pages/login.robot

*** Keywords ***
Sign In The Website
    [Arguments]         ${Username}     ${Password}
    login.Input Username  ${Username}
    login.Input Password  ${Password}
    login.Click Sign In