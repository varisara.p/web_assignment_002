*** Settings ***
Library         SeleniumLibrary

Resource        ${CURDIR}/../pages/openTrees.robot

*** Keywords ***
Open Trees Page 
    [Arguments]         ${input_value}   ${word_to_check}   ${name_of_painter}
    openTrees.Wait and Check That Have Trees And Blad3 Is THe Paint On Page   ${input_value}    ${word_to_check}  ${name_of_painter}
    openTrees.Open Tree Website To Complete Response   ${input_value}   